# PHP Tutorial

Rasmus Lerdorf unleashed the first version of PHP in 1994.

#Notes:

1 - Manage dynamic content, databases, session tracking, e-commerce sites
2 - Integrated in : MySQL PostgreSQL Oracle Microsoft SQL Server
3 - Syntax like C

#Uses:

1 - Handle forms [ Gather data from files / save to file ]
2 - Add/delete/Modify element in database
3 - Access cookies variables and set cookies
4 - Encrypt data
